
Changelog
=========

0.2.1 (2021-04-18)
------------------

* Added None in card brand
* Fixed some bugs.

0.2.0 (2021-04-18)
------------------

* Added a test.
* Fixed some bugs.

0.1.0 (2021-04-18)
------------------

* Added all API methods.
* Updated Docs.

0.0.2 (2021-04-16)
------------------

* Removed a Classifier

0.0.1 (2021-04-16)
------------------

* DataClasses for PaymentInit and APN request and responses.
* Functional Client only with session initiation.


0.0.0 (2021-04-16)
------------------

* First release on PyPI.
